const xml2js = require("xml2js");
const { promisify } = require("util");
const BaseFormat = require("./BaseFormat");

const parser = new xml2js.Parser();
const builder = new xml2js.Builder();

const parseString = promisify(parser.parseString);

class AtomFormat extends BaseFormat {
  async patern(data) {
    return data;
  }
}

module.exports = AtomFormat;
