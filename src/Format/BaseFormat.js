const xml2js = require("xml2js");
const { promisify } = require("util");
const parser = new xml2js.Parser();
const builder = new xml2js.Builder();

const parseString = promisify(parser.parseString);

class BaseFormat {
  async parse(data) {
    try {
      const xml = builder.buildObject(data);
      return xml;
    } catch (error) {
      throw "Invalid formated";
    }
  }

  async render(data) {
    try {
      const res = await parseString(data);
      return res.root;
    } catch (error) {
      throw "Invalid parsed";
    }
  }

  async format(data) {
    return data;
  }

  async transform(xml) {
    const obj = await this.parse(xml);
    const resObj = await this.format(obj);
    return await this.render(resObj);
  }
}

module.exports = BaseFormat;
