const AtomFormat = require("./AtomFormat");
const RssFormat = require("./RssFormat");

const factoryFormat = type => {
  switch (type.toUpperCase()) {
    case "ATOM":
      return new AtomFormat();
    case "RSS":
      return new RssFormat();
  }
};

module.exports = factoryFormat;
