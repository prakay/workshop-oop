const factoryReader = require("./Reader");

class Downloader {
  constructor(typeReader) {
    this.reader = factoryReader(typeReader);
  }

  async load(target) {
    return await this.reader.read(target);
  }
}

module.exports = Downloader;
