const axios = require("axios");

class ReaderRequest {
  async read(url) {
    try {
      const res = await axios.get(url);
      return res.data;
    } catch (error) {
      throw "Invalid request";
    }
  }
}

module.exports = ReaderRequest;
