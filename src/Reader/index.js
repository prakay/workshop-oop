const fs = require("fs");
const ReaderRequest = require("./ReaderRequest");
const ReaderFile = require("./ReaderFile");

const factoryReader = typeReader => {
  switch (typeReader.toUpperCase()) {
    case "URL":
      return new ReaderRequest();
    case "FILE":
      return new ReaderFile();
  }
};

module.exports = factoryReader;
