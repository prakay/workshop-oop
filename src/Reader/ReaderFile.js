const fsp = require("fs").promises;

class ReaderFile {
  async read(path) {
    try {
      const data = await fsp.readFile(path, "utf8");
      return data;
    } catch (error) {
      throw "Invalid read file";
    }
  }
}

module.exports = ReaderFile;
