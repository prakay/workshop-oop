const program = require("commander");
const Converter = require("../");

program.version("0.1.0").description("Parser");

program
  .usage("[options] <file|url>")
  .option("-o, --output <rss|atom>", "output format", /^(rss|atom)$/i, "rss")
  .option("-t, --type <url|file>", "input format", /^(url|file)$/i, "file")
  .action(async (dir, cmd) => {
    const converter = new Converter();

    const res = await converter.convert(dir, {
      output: cmd.output,
      type: cmd.type
    });

    console.log("res", res);
  });

program.parse(process.argv);
