const factoryFormat = require("./Format");
const Downloader = require("./Downloader");

class Converter {
  async convert(dir, options) {
    try {
      const downloader = new Downloader(options.type);
      const data = await downloader.load(dir);

      const format = factoryFormat(options.output);

      const res = await format.transform(data);

      return res;
    } catch (error) {
      return error;
    }
  }
}

module.exports = Converter;
