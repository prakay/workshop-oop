const axios = require("axios");
const moxios = require("moxios");
const fsp = require("fs").promises;
const Converter = require("../src");
const { getFixturePathSync } = require("jest-fixtures");

describe("mocking axios requests", () => {
  beforeEach(function() {
    moxios.install();
  });

  afterEach(function() {
    moxios.uninstall();
  });

  it("tests error with rejects load file", async () => {
    const example = await fsp.readFile(
      getFixturePathSync(__dirname, "example.xml"),
      "utf8"
    );

    moxios.stubRequest("http://my-xml.ru", {
      status: 200,
      response: example
    });

    const converter = new Converter();
    const res = await converter.convert("http://my-xml.ru", {
      output: "atom",
      type: "url"
    });

    expect(res).toEqual(example);
  });

  it("tests error with rejects read file", async () => {
    const example = await fsp.readFile(
      getFixturePathSync(__dirname, "example.xml"),
      "utf8"
    );

    const converter = new Converter();
    const res = await converter.convert("./examples/rss.xml", {
      output: "atom",
      type: "file"
    });

    expect(res).toEqual(example);
  });
});
